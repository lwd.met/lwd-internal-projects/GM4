# -*- coding: utf-8 -*-
"""
Created on Tue Oct 12 11:37:56 2021

@author: haroh
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Apr 15 08:22:29 2021

@author: haroh
"""

import os
import matplotlib.pyplot as plt
import geopandas as gpd
import earthpy as et
import pandas as pd
import numpy as np
from scipy.spatial import cKDTree
from shapely.geometry import Point 

#%% define path and for all layers 
path_staatsgrenze = '/home/hanna/GITLAB/GM4/Karte/Karte Tirol/staatsgrenze/staat_l_z7_tirol.shp'
path_haupterverkehrswege = '/home/hanna/GITLAB/GM4/Karte/Karte Tirol/Straßen/hauptverkehrswege_tirol.shp'
path_orte = '/home/hanna/GITLAB/GM4/Karte/Karte Tirol/Orte.shp'
path_snowpackstation = '/home/hanna/GITLAB/GM4/Karte/Karte Tirol/snowpack stations/lookuptable_selectedstations201718.csv'
path_provinzgrenzen = '/home/hanna/GITLAB/GM4/Karte/Karte Tirol/staatsgrenze/provinzen_l_tirol.shp'
path_albina_regions = '/home/hanna/GITLAB/GM4/Karte/Karte Tirol/Regions_LWD/AT-07_micro-regions.geojson.json'

#create dataframe for all layers - make sure ever gpd.df has the same projection
data_staatsgrenze = gpd.read_file(path_staatsgrenze).set_crs('epsg:4326', allow_override=True)
data_haupterverkehrswege = gpd.read_file(path_haupterverkehrswege).set_crs('epsg:4326', allow_override=True)
data_orte = gpd.read_file(path_orte).set_crs('epsg:4326', allow_override=True)
data_provinzgrenzen = gpd.read_file(path_provinzgrenzen).set_crs('epsg:4326', allow_override=True)
data_albina_regions = gpd.read_file(path_albina_regions).set_crs('epsg:4326', allow_override=True)
#data_snowpackstation:
stations = pd.read_csv(path_snowpackstation, sep=',')#,encoding='unicode_escape')
#stations = stations.astype({'longitude':float})

# creating a geometry column 
geometry = [Point(xy) for xy in zip(stations['longitude'], stations['latitude'])]
# Coordinate reference system : WGS84
crs = {'init': 'epsg:4326'}
# Creating a Geoeopandas geographic data frame 
data_SnPstat_temp  = gpd.GeoDataFrame(stations, crs=crs, geometry=geometry)
#rename LWDNUM - ISEE1 --> ISEE | ISEE2 --> ISEE 
data_SnPstat_temp.LWDNummer = data_SnPstat_temp.LWDNummer.str.replace('1','').str.strip()
data_SnPstat_temp.LWDNummer = data_SnPstat_temp.LWDNummer.str.replace('2','').str.strip()
data_SnPstat_temp.LWDNummer = data_SnPstat_temp.LWDNummer.str.replace('3','').str.strip()
data_snowpackstation = data_SnPstat_temp.drop_duplicates(subset='LWDNummer')


#%% functions
def pip(points, polys=data_albina_regions, crs_points="epsg:4326"):
    """
    pip... Point in Polygon
    Spatial join with points in polygon.


    Parameters
    ----------
    points : Pandas DataFrame
        Points. Requires: "longitude" and "latitude" as column names.
    polys : GeoPandas DataFrame, optional
        Polygons. The default is albina_regions.
    crs_points : str, optiona  l
        Projection of the points. The default is "epsg:4326".
        see https://geopandas.org/docs/user_guide/projections.html


    Returns
    -------
    GeoPandas DataFrame
        Join of DataFrame points with respective polygons.

    """


    # Create GeoDataFrame
    points = gpd.GeoDataFrame(points, crs=crs_points,
                              geometry=gpd.points_from_xy(points.long,
                                                          points.lat))

    # Make sure poinits have same projection as polys
    points = points.to_crs(polys.crs)

    # Spatial join: points in polys
    points_join = gpd.sjoin(points, polys, how='inner', op='within')

    return(points_join)


def ckdnearest(gdA, gdB):
    '''
    get nearest distance between every point in gdA to any point in gdB

    Parameters
    ----------
    gdA : gpd.Dataframe
        dataframe including  geoinformation about profiles
    gdB : gpd.Dataframe
        dataframe including  geoinformation about SNOWPACKSTATIONS
.

    Returns
    -------
    gdf : gpd.Dataframe
        datafream icluding nearest distance and nearest neigbour   

    '''

    nA = np.array(list(gdA.geometry.apply(lambda x: (x.x, x.y))))
    nB = np.array(list(gdB.geometry.apply(lambda x: (x.x, x.y))))
    btree = cKDTree(nB)
    dist, idx = btree.query(nA, k=1)
    gdB_nearest = gdB.iloc[idx].drop(columns="geometry").reset_index(drop=True)
    gdf = pd.concat(
        [
            gdA.reset_index(drop=True),
            gdB_nearest,
            pd.Series(dist, name='dist')
        ], 
        axis=1)

    return gdf

#%%
#folder where python can find a list of profiles per season and further creat 
#a list cointaining a  csv(listing all profiles of one season) of every season
#path_profiles = r'C:\Users\haroh\OneDrive\Arbeit LWD-DESKTOP-LP46IDF\Masterarbeit\Daten\Profiles QK discarded/profilesperseason/'
path_profiles = '/home/hanna/GITLAB/GM4/smallest-distance/input'

folder = os.listdir(path_profiles)
#df_nearst = pd.DataFrame()

for season in folder:
    #emptydataframe for saving nearst distance per season 
    df_nearst = pd.DataFrame()
    df_season = pd.read_csv(path_profiles+ ('/') +str(season), header=0,sep=';', low_memory=False)  
    #create dataframe which includes all geo infromation for each snowprofile
    headers = ['id', 'date', 'elevation', 'long', 'lat', 'aspect', 'phase', 'QK']
    df_geoinfo =  pd.concat([df_season.id, df_season.Date ,df_season.Elevation, df_season.long,df_season.lat, df_season.Aspect, df_season.Phase, df_season.QK], axis=1, keys=headers)
    #df_geoinfo = df_geoinfo.astype('float').copy() 


    #rename columns to fit in function
    data_snowpackstation.columns = ['station_name', 'alititude', 'LWDNummer','lat', 'long', 'ID','geometry']
    
    #assign every profile an albina region 
    profiles_join = pip(df_geoinfo, polys=data_albina_regions, crs_points="epsg:4326")
    #assign every weatherstation an albina region in Tyrol
    #select only stations within Tyrol
    stations_join = pip(data_snowpackstation, polys=data_albina_regions, crs_points="epsg:4326")
    
    #all warningregions where profile occurence in season
    warningregions = profiles_join.drop_duplicates(subset=['id_right'])['id_right']
    
    #select profiles and stations in Region by checking index
    for region in warningregions:
        #select profiles in Region
        profiles_region = profiles_join.loc[profiles_join['id_right'] == str(region)]
        #select stations in Region
        if str(region) in stations_join['id']:    
            stations_region = stations_join.loc[stations_join['id'] == str(region)]
        else:
            stations_region = data_snowpackstation
        
        nearst = ckdnearest(profiles_region, stations_region)
        #convert degree into meter: 
        #how far is one degree --> 111139m But this is all low precision.
        nearst['dist']= nearst['dist'] * 111139
        #nearst.drop(columns=['index_right'])
 
        df_nearst = pd.concat([df_nearst, nearst])
        df_nearst.reset_index(drop=True, inplace=True)
        #df_nearst.drop(columns=['index_right'])
        
    #rename columns: since doubble name occured due to concat
    df_nearst.columns = ['id_profile', 'Date' ,'elev_profile', 'long_profile', 
                          'lat_profile', 'aspect' ,'Phase', 'QK','geometry', 'index_snp',
                          'microregion_snps', 'station_name', 'alititude_snps',
                          'lwdnummer','lat_snps', 'long_snps',
                          'ID', 'dist']

    #save as csv
    df_nearst.to_csv('/home/hanna/GITLAB/GM4/smallest-distance/output/distancetoSPS_{}'.format(str(season[23::])),sep=';')        

        
    # plot layers all together 
    #%config InlineBackend.figure_format = 'retina'
    
    
    
    fig, ax = plt.subplots(figsize = (80,64))
    #plt.figure(figsize = (80,64), dpi = 160) 
    
    ax = data_albina_regions.plot(color = 'white', edgecolor='k', linestyle = 'dotted')
    ax1 = data_staatsgrenze.plot(edgecolor='k',linewidth = 2,ax=ax)
    ax2 = data_provinzgrenzen.plot(edgecolor='k',linewidth = 1,ax=ax)
    ax3 = stations_join.plot(color='red',markersize=2, ax=ax)
    ax4 = profiles_join.plot(color = 'grey', marker='+', markersize = 0.3, ax = ax)
    for x, y, label in zip(stations_join.geometry.x, stations_join.geometry.y, stations_join.LWDNummer):
        ax3.annotate(label, xy=(x,y), size = 4)
        
    for idx, profile in enumerate(df_nearst.id_profile)   :
        x_values = [df_nearst.geometry.x[idx],df_nearst.long_snps[idx]]
        y_values = [df_nearst.geometry.y[idx], df_nearst.lat_snps[idx]]
        plt.plot(x_values, y_values, linestyle = 'solid', color = 'black', linewidth = 0.3)
    
    #plt.plot(df_geoinfo.long, df_geoinfo.lat, 'o', color = 'grey', markersize = 1 )
    plt.show()

   
#plt.plot(df_geoinfo.long, df_geoinfo.lat, 'o', color = 'grey', markersize = 1 )    
#plt.show()
#%%
fig, ax = plt.subplots(figsize = (80,64))
#plt.figure(figsize = (80,64), dpi = 160) 

ax = data_albina_regions.plot(color = 'white', edgecolor='k', linestyle = 'dotted')
ax1 = data_staatsgrenze.plot(edgecolor='k',linewidth = 2,ax=ax)
ax2 = data_provinzgrenzen.plot(edgecolor='k',linewidth = 1,ax=ax)
ax3 = stations_join.plot(color='red',markersize=2, ax=ax)
#ax4 = profiles_join.plot(color = 'grey', markersize = 1, ax = ax)
for x, y, label in zip(stations_join.geometry.x, stations_join.geometry.y, stations_join.LWDNummer):
    ax3.annotate(label, xy=(x,y), size = 4)