# Waters
- gewaesser_a: Seen (Ausschnitt AT, Nord-IT, CH, Sued-D)
- gewaesser_a_z7: Seen europaweit
- gewaesser_l: Fluesse (Ausschnitt AT, Nord-IT, CH, Sued-D)
- gewaesser_l_z7: Fluesse (zu erweitern)
- kueste_l_z7: Kuestenlinien weltweit
- ne_10m_ocean: Ozean-Polyggon weltweit
- ocean_a_z7: Ozean-Polyggon weltweit, falscher Projektion?

## Fluesse erweitern
- gis_osm_waterways_free_1 (Geofabrik)
- Attribut fclass river
- Attribute: style: 2011, category: 3, OEROK: 0 (1 bei sehr großen Flüssen), level_6 - 8: 0, level _9,10: 1 (bei sehr detailierten)
- groessere sind manchmal doppelt, sehr detailierte aussortieren