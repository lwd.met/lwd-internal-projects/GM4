# Label
- albina_namengut_l: Label (in Form von Lines) für 
	- Flüsse (style 4011)
	- style 6010 wird kaum angezeigt
	- Gebirgszüge (style 6020)
	- Täler (style 6030)
	--> muss erweitert werden
- albina_namengut_p: Label (in Form von Points) für 
	- Hauptstädte (style 1100) (weltweit vorhanden)
	- große Städte (style 1110, 1120, 1130) -> für Level 6  (weltweit vorhanden)
	- Seen (style 5011) (muss erweitert werden) 
	- Ländernamen (style 5100) (weltweit vorhanden)
	- Regionsnamen (style 5110)
	- Gebirgszüge (style 5501) (wird nicht in Karte angezeigt)
	
## Labels erweitern
manuell hinzufügen	