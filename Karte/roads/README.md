# Strassen
- hauptverkehrswege_l: grosse Strassen, Autobahnen, Fernstrassen (zu erweitern)
- residential: kleine Strassen in Oesterreich
- roads: kleine Strassen in Italien
- secondary_roads: sekundäre Strassen in Oesterreich
- tertiary_roads: tertiäre Strassen in Oesterreich

## hauptverkehrswege_l erweitern
- Shape-Files: gis_osm_roads_free_1 (Geofabrik)
- Attribut fclass: motorway + manuelle Auswahl von trunk und primary
- Attribute: style:2311 (Motorway) 2321 (anderer große Straßen), level_6,7,8: 0, level_9:abh von Größe, level_10=1