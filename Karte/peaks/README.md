# Gipfel
- Peaks_major_new: Gipfel (zu erweitern)
- South_Tyrol_peaks: Gipfel in Suedtirol
- Trentino_Peaks: Gipfel in Trentino
- albina_hoehenpunkte: Gipfel (werden nicht in Karte dargestellt?)
- peak_major: Gipfel in Oesterreich (weniger als peak_minor)
- peak_minor: Gipfel in Oesterreich


## Gipfel erweitern: in Peaks_major_new
- aus gis_osm_landuse_a_free_1 (Geofabrik)
- Attribut fclass: peak
- Manuell nur einzelne Gipfel wählen (mit Hilfe von Outdooractive-Karte)
- Attribute: height maunell hinzugügen (mit Outdooractive); style: 1010; level_9/10 manuell wählen;