The goal is to use a hand-filtered dataset representing the GM4 weak layer in snow profiles to map the geographic location of the profiles and their location to the surrounding SNOWPACK measurement stations.

In further steps, the snow cover will be simulated at the selected stations.  If the respective weak layer is modeled, the snow profiles from SNOWPACK can also be added to the data collection.

Furthermore, it is a goal to automate the previously manually performed data collection.

directory "Karte":
All documents for the graphical illustration of the data points are stored in this folder. The map was taken from the ALBINA PROJECT and adapted in the folder "Karte Tirol".
Therfore layers - "*_tirol" are those layers that are adapted to the map section tirol.  
In the folder snowprofiles you can find the geoinformations and addditional Notes of the hand-filtered GM4 DATASET in the "*.csv" files 
"geo_info_snpwpackstations.csv" inclueds the geoinformations of functional snowpack stations in the season 2019/20 <-- maybe we need to adapt this file a little bit
"albina_base_map_2.qgz" shows the result


"reads-pro-file.py"gives me the feoinformation from the outputfile of snowpack - maybe wenn need this in further steps


Qualitätsklassen: 
1.)  Stabilit�tstest wei�t auf Schwachschicht in dieser b  
     Schicht hin
                        UND 
     Erfahrener Beobachter hinterlie� Notiz         
   
2.)   Stabilit�tstest wei�t auf Schwachschicht in dieser 
      Schicht hin
                        UND 
      Muster ist Charakteristisch - (Sandwichkruste oder unter
      Saharastaub) 
 				UND
  [   liegt im ausgegebenen H�henband und Exposition 
                        ODER
     	Profil wurde in der N�he eines Unfalls welcher in Folge 
  	einer GM4 Schawchschicht passierte 
     	ausgenommen      ]

3.)   Erfahrener Beobachter hinterlie� Notiz 
 
4.)  	Muster ist Charakteristisch - (Sandwichkruste oder 
 	unter Saharastaub) 
				UND
[    liegt im ausgegebenen H�henband und Exposition 	                        
				ODER
     	Profil wurde in der N�he eines Unfalls welcher in Folge 
	einer GM4 Schawchschicht passierte 
     	aufgenommen      ]

5.) 	Profil liegt im ausgegebenen H�henband und Exposition 	                        
				ODER
     	Profil wurde in der N�he eines Unfalls welcher in Folge 
	einer GM4 Schawchschicht passierte 
     	aufgenommen      

 
6.) 	Stabilit�tstest wei�t auf Schwachschicht in dieser 
 	Schicht hin
