# -*- coding: utf-8 -*-
"""
This skript includes only functions: 
    download        : save all xml files from LAWIS in the selected time 
                      period(range start, stop) in chosen directory(path) 
    preprocessing   : parase xml snowprofile - trace back 2 DataFrames 
                      with with values of interest (snowprofiles in Dataframe
                      format and meta data)

    GM4             : ask dataframe for DP4 weak layers. Trace back a variable
                      which inclueds informations wetaher there is cold after 
                      warm or warm after cold weak layer. If there is a weak 
                      layer it also points out at which height it has been 
                      identified. 

@author: haroh
"""

#import packages
import requests
import os.path
import numpy as np
import xml.etree.ElementTree as et
import pandas as pd
import datetime
from datetime import datetime
    


def preprocoessing(profil_id):
    """
    data = requests.get("https://lawis.at/lawis_api/v2_2/profile/" ).json()

    stop = '2018-10-01'
    start = '2019-05-31'
    
    """
    
    identity = profil_id

    
    
    url_layer = 'https://www.lawis.at/lawis_api/normalizer/profile/{}/schichten?lang=de'.format(profil_id)
    data_layers = requests.get(url_layer).json()

    url_metadata = 'https://lawis.at/lawis_api/normalizer/profile/{}?lang=de'.format(profil_id)
    data_meta = requests.get(url_metadata).json()
    
    #creates empty lists for layers
    layerstart = []
    layerend = []
    thickness = []
    humidity = []
    grainFormPrimary = []
    grainFormSecondary = []
    hardness = []
    
    #creates empty lists for meta data
    identity = []
    recordingdate = []
    name = []
    region = []
    subregion_id =  []
    location = []
    altitude = []
    aspect = []
    slopeangle =  []
    long = []
    lat = []
    for layer in range(0, len(data_layers)): 
        layerstart.append(data_layers[layer]['bishoehe'])
        layerend.append(data_layers[layer]['vonhoehe'])
        thickness.append(float(data_layers[layer]['vonhoehe']) - float(data_layers[layer]['bishoehe']))
        humidity.append(data_layers[layer]['feuchte'])
        grainFormPrimary.append(data_layers[layer]['kornform1'])
        grainFormSecondary.append(data_layers[layer]['kornform2'])
        hardness.append(data_layers[layer]['haerte']['value'])
    
        
    region.append(data_meta['region_id'])
    location.append(data_meta['ort'])
    subregion_id.append(data_meta['subregion_id'])
    identity.append(data_meta['id'])
    name.append(data_meta['name'])
    recordingdate.append(data_meta['profildatum'])
    altitude.append(data_meta['seehoehe'])
    long.append(data_meta['longitude'])
    lat.append(data_meta['latitude'])
    slopeangle.append(data_meta['hangneigung'])
    aspect.append(data_meta['exposition_id'])

    # Convert str of hardness to float
    # 1 = F- = Faust
    # 1-2 = F+
    # 2 = 4F = 4Finger
    # 3 = 1F = 1Finger
    # 4-4 = 1F+
    # 4 = P = Bleistift
    # 4-5 = P+
    # 5 = K = Knife 
    # 5-6 = M- 
    # 6 = I = Ice

    for idx, layer_str in enumerate(hardness):
        if layer_str == '1':
            hardness[idx] = 1
        elif layer_str == '1-2':
            hardness[idx] = 1.5
        elif layer_str == '2':
            hardness[idx] = 2
        elif layer_str == '2-3':
            hardness[idx] = 2.5
        elif layer_str == '3':
            hardness[idx] = 3
        elif layer_str == '3-4':
            hardness[idx] = 3.5
        elif layer_str == '4':
            hardness[idx] = 4
        elif layer_str == '4-5':
            hardness[idx] = 4.5
        elif layer_str == '5':
            hardness[idx] = 5  
        elif layer_str == '5-6':
            hardness[idx] = 5.5 
        elif layer_str == '6':
            hardness[idx] = 6  
    
    # Convert str of grainform to shortcut
    # Precip particles = Neuschnee = PP
    # decomp/fragm = Filziger Schnee = df
    # Rounded Grains = Rundkörnig = RG
    # faceted cystals = Kantigkörnig = FC
    # Depth hoar= Tiefenreif = DH
    # Surface hoar = Oberflächenreif = SH
    # Meltforms = Schmelzform = MF
    # kantig, abgerundet  = faceted rounded FCxr
    # Graupel = Graupel = PPgp
    # Schmelzkruste = Melt freez crust = MFcr
    
    for idx, grainForm in enumerate(grainFormPrimary):
        if grainForm == 'Neuschnee':
            grainFormPrimary[idx] = 'PP'
        elif grainForm == 'Filziger Schnee':
            grainFormPrimary[idx] = 'df'
        elif grainForm == 'Rundkörnig':
            grainFormPrimary[idx] = 'RG'
        elif grainForm == 'Kantigkörnig':
            grainFormPrimary[idx] = 'FC'
        elif grainForm == 'Tiefenreif':
            grainFormPrimary[idx] = 'DH'
        elif grainForm == 'Oberflächenreif':
            grainFormPrimary[idx] = 'SH'
        elif grainForm == 'Schmelzform':
            grainFormPrimary[idx] = 'MF'
        elif grainForm == 'kantig, abgerundet':
            grainFormPrimary[idx] = 'FCxr'
        elif grainForm == 'Graupel':
            grainFormPrimary[idx] = 'PPgp'  
        elif grainForm == 'Schmelzkruste':
            grainFormPrimary[idx] = 'MFcr'
        elif grainForm == 'Eislamelle':
            grainFormPrimary[idx] = '-' 
    
    for idx, grainForm2 in enumerate(grainFormSecondary):
        if grainForm2 == 'Neuschnee':
            grainFormSecondary[idx] = 'PP'
        elif grainForm2 == 'Filziger Schnee':
            grainFormSecondary[idx] = 'df'
        elif grainForm2 == 'Rundkörnig':
            grainFormSecondary[idx] = 'RG'
        elif grainForm2 == 'Kantigkörnig':
            grainFormSecondary[idx] = 'FC'
        elif grainForm2 == 'Tiefenreif':
            grainFormSecondary[idx] = 'DH'
        elif grainForm2 == 'Oberflächenreif':
            grainFormSecondary[idx] = 'SH'
        elif grainForm2 == 'Schmelzform':
            grainFormSecondary[idx] = 'MF'
        elif grainForm2 == 'kantig, abgerundet':
            grainFormSecondary[idx] = 'FCxr'
        elif grainForm2 == 'Graupel':
            grainFormSecondary[idx] = 'PPgp'  
        elif grainForm2 == 'Schmelzkruste':
            grainFormSecondary[idx] = 'MFcr'
        elif grainForm2 == 'Eislamelle':
            grainFormSecondary[idx] = '-' 
    
    


    df_md = pd.DataFrame({'identity': identity ,'Date':recordingdate,
                          'Name':name, 'region':region, 
                          'subregion':subregion_id, 'location':location, 
                          'long': long , 'lat': lat, 'Elevation':altitude, 
                          'Aspect':aspect, 'SlopeAngle': slopeangle}) 

    
        
    df = pd.DataFrame({'Hmin': layerstart,'Hmax':layerend, 'humidity': humidity,
                        'grainFormPrimary':grainFormPrimary,
                        'grainFormSecondary':grainFormSecondary,
                        'hardness':hardness})
    
    # df = pd.DataFrame({'humidity': humidity,
    #                     'grainFormPrimary':grainFormPrimary,
    #                     'grainFormSecondary':grainFormSecondary,
    #                     'hardness':hardness})
    return df_md, df



#%% 
   
    

def DP4(df, hardness): 
    '''
    ask dataframe for DP4 weak layers. Trace back a variable which inclueds 
    informations wetaher there is cold after warm or warm after cold weak layer. 
    If there is a weak layer it also points out at which height it has been
    identified. 

    Parameters
    ----------
    df : pd.DataFrame

    Returns
    -------
    coldafterwarm : str
        includes informations wetaher there is a cold after warm weak layer
    warmaftercold : str
        includes informations wetaher there is a warm after cold weak layer
    '''
    coldafterwarm = 'no cold after warm found'
    warmaftercold = 'no warm after cold found'
    
    #loop through DataFrame which includes the snowprofile.
    #Ask for Meltfreezcrust. If there is one ask if there is something 
    #faceted around
    for idx, layer in enumerate(df.grainFormPrimary):
        if layer == 'MFcr':
            #checking if MFcr is the snowsurfacelayer
            #otherwise cheking grainforms above. If it is something faceted
            #variable "coldafterwarm" or "warmaftercold" is set to
            #"YES at ('height of the weak layer')"
            if idx < len(df.grainFormPrimary)-1:                    
                if df.hardness[idx+1] <= hardness:  
                    if df.grainFormPrimary[idx+1] == 'FC':
                        warmaftercold = 'yes at' + str(df.Hmax[idx])
                    elif df.grainFormPrimary[idx+1] == 'FCxr':
                        warmaftercold = 'yes at' + str(df.Hax[idx])
                    elif df.grainFormPrimary[idx+1] == 'DH':
                        warmaftercold = 'yes at' + str(df.Hmax[idx])
                        
                    #chcek grainFormSecondary
                    elif df.grainFormSecondary[idx+1] == 'FC':
                        warmaftercold = 'yes at' + str(df.Hmax[idx])
                    elif df.grainFormSecondary[idx+1] == 'FCxr':
                        warmaftercold = 'yes at' + str(df.Hax[idx])
                    elif df.grainFormSecondary[idx+1] == 'DH':
                        warmaftercold = 'yes at' + str(df.Hmax[idx])
                    
                    #commented because we do not want surface hoar above CRUST
                    #elif df.grainFormPrimary[idx+1] == 'SH':
                        #if df.grainFormSecondary[idx+1] == 'FCxr' or 'FC' or 'DH'or 'SH': 
                            #warmaftercold = 'yes at' + str(df.Hmax[idx])
                    else:
                        pass
            else:
                pass
               #print('this is the surfcae layer')
                    
            #checking if MFcr is the bottom layer
            #otherwise cheking grainforms underneath. If it is something faceted
            #variable "coldafterwarm" or "warmaftercold" is set to
            #"YES at ('height of the weak layer')"
            if idx > 0:
                if df.hardness[idx-1] <= hardness: 
                    if df.grainFormPrimary[idx-1] == 'FC':
                        coldafterwarm = 'yes at' + str(df.Hmin[idx])                           
                    elif df.grainFormPrimary[idx-1] == 'FCxr':
                        coldafterwarm = 'yes at' + str(df.Hmin[idx])
                    elif df.grainFormPrimary[idx-1] == 'DH':
                        coldafterwarm = 'yes at' + str(df.Hmin[idx])
                    #chcek grainFormSecondary
                    elif df.grainFormSecondary[idx-1] == 'FC':
                        coldafterwarm = 'yes at' + str(df.Hmax[idx])
                    elif df.grainFormSecondary[idx-1] == 'FCxr':
                        coldafterwarm = 'yes at' + str(df.Hax[idx])
                    elif df.grainFormSecondary[idx-1] == 'DH':
                        coldafterwarm = 'yes at' + str(df.Hmax[idx])

                    else:
                        pass
            else:
                pass
                #print('This is the bottom  layer')

                
        #check if MF is is misinterpreted as MFcr        
        elif layer == 'MF':
            if df.hardness[idx] >= 3:
                #checking if misinterpretet MF is the snowsurfacelayer
                #otherwise cheking grainforms above. If it is something faceted
                #variable "coldafterwarm" or "warmaftercold" is set to
                #"YES at ('height of the weak layer')"
                if idx < len(df.grainFormPrimary)-1:          
                    if df.hardness[idx+1] <= hardness:  
                        if df.grainFormPrimary[idx+1] == 'FC':
                            warmaftercold = 'yes at' + str(df.Hmax[idx])        
                        elif df.grainFormPrimary[idx+1] == 'FCxr':
                            warmaftercold = 'yes at' + str(df.Hmax[idx])
                        elif df.grainFormPrimary[idx+1] == 'DH':
                            warmaftercold = 'yes at' + str(df.Hmax[idx])
                            
                        #chcek grainFormSecondary
                        elif df.grainFormSecondary[idx+1] == 'FC':
                            warmaftercold = 'yes at' + str(df.Hmax[idx])
                        elif df.grainFormSecondary[idx+1] == 'FCxr':
                            warmaftercold = 'yes at' + str(df.Hax[idx])
                        elif df.grainFormSecondary[idx+1] == 'DH':
                            warmaftercold = 'yes at' + str(df.Hmax[idx])                            
                        else:
                            pass
                            
                    else:
                        pass
                else:
                    pass
                    #print('this is the surfcae layer')
                    
                #checking if misinterpretet MF is the bottom layer
                #otherwise cheking grainforms underneath. If it is something 
                #faceted variable "coldafterwarm" or "warmaftercold" is set to
                #"YES at ('height of the weak layer')"
                if idx > 0:
                    if df.hardness[idx-1] <= hardness: 
                        if df.grainFormPrimary[idx-1] == 'FC':
                            coldafterwarm = 'yes at' + str(df.Hmin[idx])
                        elif df.grainFormPrimary[idx-1] == 'FCxr':
                            coldafterwarm = 'yes at' + str(df.Hmin[idx])
                        elif df.grainFormPrimary[idx-1] == 'DH':
                            coldafterwarm = 'yes at' + str(df.Hmin[idx])
                        #chcek grainFormSecondary
                        elif df.grainFormSecondary[idx-1] == 'FC':
                            coldafterwarm = 'yes at' + str(df.Hmax[idx])
                        elif df.grainFormSecondary[idx-1] == 'FCxr':
                            coldafterwarm = 'yes at' + str(df.Hax[idx])
                        elif df.grainFormSecondary[idx-1] == 'DH':
                            coldafterwarm = 'yes at' + str(df.Hmax[idx])
                        else:
                            pass
                else:
                    pass
                    #print('This is the bottom  layer')
            else: 
                #not misinterpreted
                pass

        else:
            pass
            #print('this layer does not indicate a typical Gm4 pattern') 
                
    return(warmaftercold, coldafterwarm)
