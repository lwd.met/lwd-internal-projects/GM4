# -*- coding: utf-8 -*-
"""
This skript includes only functions: 
    download        : save all xml files from LAWIS in the selected time 
                      period(range start, stop) in chosen directory(path) 
    preprocessing   : parase xml snowprofile - trace back 2 DataFrames 
                      with with values of interest (snowprofiles in Dataframe
                      format and meta data)

    GM4             : ask dataframe for DP4 weak layers. Trace back a variable
                      which inclueds informations wetaher there is cold after 
                      warm or warm after cold weak layer. If there is a weak 
                      layer it also points out at which height it has been 
                      identified. 

@author: haroh
"""

#import packages
import requests
import os.path
import numpy as np
import xml.etree.ElementTree as et
import pandas as pd


def download(start, stop, path):
    '''
    save all xml files from LAWIS in the selected time 
    period(range start, stop) in chosen directory 
    
    Parameters
    ----------
    start : int
        number of the snow profile where the download should start
        should be checked in LAWIS before 
    stop : int
        number of the snow profile where the download should end
        should be checked in LAWIS before
    path : str
        directory where the downloaded profiles should be saved
            
    Returns
    -------
    folder with *.xml files for DP4 analysis

    '''
    #define amount of profiels for download - 
    #number need to be checked in lawis per hand
    beginn = start
    end = stop
    span  = np.arange(beginn, end, 1)

    
    for profile in span:
        url = 'https://www.lawis.at/lawis_api/public/profile/{}?format=xml'.format(profile)
        res = requests.get(url, allow_redirects=True)
        try:
            res.raise_for_status()
    
            file = 'snowprofile{}.xml'.format(profile)
            path_new = os.path.join(path, file)
            open(str(path_new), 'wb').write(res.content)
        except:
            profile = profile + 1
    
    return 

#%%
def preprocessing(file_name, pathtofile):
    '''
    parase xml snowprofile trace back 2 DataFrame with with values of interest

    Parameters
    ----------
    file_name : str
        name of xml file of snowprofile
    pathtofile : str
        directorypath to snowporfile

    Returns
    -------
    df : pd.DataFrame
        snowprofile in DataFrameFormat
    df_md : pd.DataFrame
        meta data of the snowprofile 
    
    '''
    #define path where profiles are stored
    pathtoparase = os.path.join(pathtofile, file_name)
    
    #parse the xml object
    tree = et.parse(pathtoparase)
    #dumping entire tree to variable xroot
    xroot = tree.getroot()

    #creates empty lists for layers
    depthTop = []
    thickness = []
    grainFormPrimary = []
    grainFormSecondary = []
    hardness = []
    date = []
    #creates empty lists for meta data
    region = []
    location = []
    elevation = []
    aspect = []
    slopeangle =  []
    geoinfo =  []
    name = []
    long = []
    lat = []
    
   
    
    #loop through xml object at each stage of the loop the iteration is saved 
    #to a variable which is the basis for the loop at the next stage (one level below). 
    #et.Element(‘root’) creates an empty xml object
   
    for child in xroot:
        xroot1 = et.Element('root1')
        xroot1 = (child)
        for grandchild in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}srcRef'):
            xroot2_0 = et.Element('root2_0')
            xroot2_0 = (grandchild)
            for elem in xroot2_0.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}name'):
                name.append(elem.text)
        for grandchild1 in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}locRef'):
            xroot2_1 = et.Element('root2_1')
            xroot2_1 = (grandchild1)
            for elem2 in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}name'):
                location.append(elem2.text)
                
            for locinforegion in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}region'):
                region.append(locinforegion.text)  
                
            for locinfo in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}validElevation'):
                xroot3_0 = et.Element('root3_0')
                xroot3_0 = (locinfo)
                for locinfoelevation in xroot3_0.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                    elevation.append(locinfoelevation.text)
            for locinfo1 in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}validAspect'):
                xroot3_1 = et.Element('root3_01')
                xroot3_1 = (locinfo1)
                for locinfoaspect in xroot3_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                    aspect.append(locinfoaspect.text)                
            for locinfo2 in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}validSlopeAngle'):
                xroot3_2 = et.Element('root3_02')
                xroot3_2 = (locinfo2)
                for locinfoslopeangle in xroot3_2.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                    slopeangle.append(locinfoslopeangle.text)                
            for locinfo3 in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}pointLocation'):
                xroot3_3 = et.Element('root3_03')
                xroot3_3 = (locinfo3)
                for locinfolong in xroot3_3.iter('{http://www.opengis.net/gml}pos'):
                    geoinfo.append(locinfolong.text)
                    long.append(locinfolong.text[0:7]) 
                    lat.append(locinfolong.text[-8:])         
             

            
        for grandchild2 in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}snowProfileResultsOf'):
            xroot2 = et.Element('root2') 
            xroot2 = grandchild2
            for measurment in xroot2.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}SnowProfileMeasurements'):
                xroot3 = et.Element('root3') 
                xroot3 = (measurment)
                for layer in xroot3.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}stratProfile'):
                    xroot4 = et.Element('root4') 
                    xroot4 = (layer)                
                    for val_depth in xroot4.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}depthTop'):
                        depthTop.append(val_depth.text)
                    for val_thickness in xroot4.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}thickness'):
                        thickness.append(val_thickness.text)
                    for form in xroot4.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}grainFormPrimary'):
                        grainFormPrimary.append(form.text)
                    for form2 in xroot4.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}grainFormSecondary'):
                        grainFormSecondary.append(form2.text)
                    for val_hardness in xroot4.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}hardness'):
                        hardness.append(val_hardness.text)

        for grandchild3 in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}timeRef'):
            xroot2_3 = et.Element('root2_03')
            xroot2_3 = (grandchild3)
            for time in xroot2_3.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}timePosition'):
                date.append(time.text)
                
        for grandchild4 in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}metaData'):
            xroot4_1 = et.Element('root4_01')
            xroot4_1 = (grandchild4)
            for data in xroot4_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}comment'):
                comments = (data.text)
                
    #design DataFrame to store values of interest                     
    df_md = pd.DataFrame(columns=['Date', 'Name', 'location', 'long' , 'lat', 
                                'Elevation', 'Aspect','SlopeAngle'])
    
    df = pd.DataFrame(columns=['depthTop','thickness','grainFormPrimary',
                               'grainFormSecondary','hardness'])
    
    #select only snowprofiles recorded Tirol
    if region[0] == 'Tirol':
  
        #dataframe_metadata
        df_md = pd.DataFrame({'Date':date, 'Name':name, 'location':location, 'long': long , 
                          'lat': lat, 'Elevation':elevation, 'Aspect':aspect,
                          'SlopeAngle': slopeangle})
                
        #dataframe_snowprofile
        df = pd.DataFrame({'depthTop': depthTop,'thickness':thickness,
                       'grainFormPrimary':grainFormPrimary,
                       'grainFormSecondary':grainFormSecondary,
                       'hardness':hardness})
    else:
        pass


    # Convert str of hardness to float
    # 1 = F- = Faust
    # 1-2 = F+
    # 2 = 4F = 4Finger
    # 3 = 1F = 1Finger
    # 4-4 = 1F+
    # 4 = P = Bleistift
    # 4-5 = P+
    # 5 = K = Knife 
    # 5-6 = M- 
    # 6 = I = Ice
    

    for idx, layer_str in enumerate(df.hardness):
        if layer_str == 'F-':
            df.hardness[idx] = 1
        elif layer_str == 'F+':
            df.hardness[idx] = 1.5
        elif layer_str == '4F':
            df.hardness[idx] = 2
        elif layer_str == '4F+':
            df.hardness[idx] = 2.5
        elif layer_str == '1F':
            df.hardness[idx] = 3
        elif layer_str == '1F+':
            df.hardness[idx] = 3.5
        elif layer_str == 'P':
            df.hardness[idx] = 4
        elif layer_str == 'P+':
            df.hardness[idx] = 4.5
        elif layer_str == 'K':
            df.hardness[idx] = 5  
        elif layer_str == 'K+':
            df.hardness[idx] = 5.5 
        elif layer_str == 'I':
            df.hardness[idx] = 6 



    
    return df, df_md, comments

    

#df, df_md = preprocessing('snowprofile6486.xml')


#%% 
#shortcuts for Snow crystal shape:
    #Precip particles = Neuschnee = PP
    #decomp/fragm = flizig = df
    #Rounded Grains = kleine Runde = RG
    #kantig = faceted cystals = FC
    #Depth hoar= Tiefenreif = DH
    #Surface hoar = Oberflächenreif = SH
    #Meltforms = Schmelzform = MF
    #kantig abgerundet = faceted rounded FCxr
    #Graupel = Graupel = PPgp
    #Schmelzkruste = Melt freez crust = MFcr


def DP4(df, hardness): 
    '''
    ask dataframe for DP4 weak layers. Trace back a variable which inclueds 
    informations wetaher there is cold after warm or warm after cold weak layer. 
    If there is a weak layer it also points out at which height it has been
    identified. 

    Parameters
    ----------
    df : pd.DataFrame

    Returns
    -------
    coldafterwarm : str
        includes informations wetaher there is a cold after warm weak layer
    warmaftercold : str
        includes informations wetaher there is a warm after cold weak layer
    '''
    coldafterwarm = 'no cold after warm found'
    warmaftercold = 'no warm after cold found'
    
    #loop through DataFrame which includes the snowprofile.
    #Ask for Meltfreezcrust. If there is one ask if there is something 
    #faceted around
    for idx, layer in enumerate(df.grainFormPrimary):
        if layer == 'MFcr':
            #checking if MFcr is the snowsurfacelayer
            #otherwise cheking grainforms above. If it is something faceted
            #variable  or "warmaftercold" is set to
            #"YES at ('height of the weak layer')"
            if idx > 0:
                if df.hardness[idx-1] <= hardness:
                    #grain Form Primary
                    if df.grainFormPrimary[idx-1] == 'FC':
                        warmaftercold = 'yes at' + str(df.depthTop[idx])                               
                    elif df.grainFormPrimary[idx-1] == 'FCxr':
                        warmaftercold = 'yes at' + str(df.depthTop[idx])
                    elif df.grainFormPrimary[idx-1] == 'DH':
                        warmaftercold = 'yes at' + str(df.depthTop[idx])
                    #grain Form Secondary
                    elif df.grainFormSecondary[idx-1] == 'FC':
                        warmaftercold = 'yes at' + str(df.depthTop[idx])                               
                    elif df.grainFormSecondary[idx-1] == 'FCxr':
                        warmaftercold = 'yes at' + str(df.depthTop[idx])
                    elif df.grainFormSecondary[idx-1] == 'DH':
                        warmaftercold = 'yes at' + str(df.depthTop[idx])
                    else:
                        warmaftercold = 'no'
            else:
                pass
               #print('this is the surfcae layer')
                    
            #checking if MFcr is the bottom layer
            #otherwise cheking grainforms underneath. If it is something faceted
            #variable "coldafterwarm"  is set to
            #"YES at ('height of the weak layer')"
            if idx < len(df.grainFormPrimary)-1:
                if df.hardness[idx+1] <= hardness: 
                    #grain Form Primary                    
                    if df.grainFormPrimary[idx+1] == 'FC':               
                        coldafterwarm = 'yes at' + str(df.depthTop[idx])                           
                    elif df.grainFormPrimary[idx+1] == 'FCxr':  
                        coldafterwarm = 'yes at' + str(df.depthTop[idx])
                    elif df.grainFormPrimary[idx+1] == 'DH': 
                        coldafterwarm = 'yes at' + str(df.depthTop[idx])
                    elif df.grainFormPrimary[idx+1] == 'SH':  
                        coldafterwarm = 'yes at' + str(df.depthTop[idx])
                    #grain Form Secondary
                    elif df.grainFormSecondary[idx+1] == 'FC':
                        warmaftercold = 'yes at' + str(df.depthTop[idx])                               
                    elif df.grainFormSecondary[idx+1] == 'FCxr':
                        warmaftercold = 'yes at' + str(df.depthTop[idx])
                    elif df.grainFormSecondary[idx+1] == 'DH':
                        warmaftercold = 'yes at' + str(df.depthTop[idx])  
                    elif df.grainFormSecondary[idx+1] == 'SH':  
                        coldafterwarm = 'yes at' + str(df.depthTop[idx])
                    else:
                        coldafterwarm = 'no'
            else:
                pass

                
        #check if MF is is misinterpreted as MFcr        
        elif layer == 'MF':
            if df.hardness[idx] >= 3:
                #checking if misinterpretet MF is the snowsurfacelayer
                #otherwise cheking grainforms above. If it is something faceted
                #variable  "warmaftercold" is set to
                #"YES at ('height of the weak layer')"
                if idx > 0:
                    if df.hardness[idx-1] <= hardness:  
                        #print('potential GM4 - lets check grainform in layer above')
                        #grain Form Primary
                        if df.grainFormPrimary[idx-1] == 'FC':
                            warmaftercold = 'yes at' + str(df.depthTop[idx])        
                        elif df.grainFormPrimary[idx-1] == 'FCxr':
                            warmaftercold = 'yes at' + str(df.depthTop[idx])
                        elif df.grainFormPrimary[idx-1] == 'DH':
                            warmaftercold = 'yes at' + str(df.depthTop[idx])
                        #grain Form Secondary
                        elif df.grainFormSecondary[idx-1] == 'FC':
                            warmaftercold = 'yes at' + str(df.depthTop[idx])                               
                        elif df.grainFormSecondary[idx-1] == 'FCxr':
                            warmaftercold = 'yes at' + str(df.depthTop[idx])
                        elif df.grainFormSecondary[idx-1] == 'DH':
                            warmaftercold = 'yes at' + str(df.depthTop[idx])
                        else:
                            warmaftercold = 'no' 
                else:
                    pass
                    
                #checking if misinterpretet MF is the bottom layer
                #otherwise cheking grainforms underneath. If it is something 
                #faceted variable "coldafterwarm"  is set to
                #"YES at ('height of the weak layer')"
                if idx < len(df.grainFormPrimary)-1:
                    if df.hardness[idx+1] <= hardness: 
                        #print('potential GM4 - lets check grainform in layer underneath')
                        #grain Form Primary
                        if df.grainFormPrimary[idx+1] == 'FC':
                            coldafterwarm = 'yes at' + str(df.depthTop[idx])
                        elif df.grainFormPrimary[idx+1] == 'FCxr':
                            coldafterwarm = 'yes at' + str(df.depthTop[idx])
                        elif df.grainFormPrimary[idx+1] == 'DH': 
                            coldafterwarm = 'yes at' + str(df.depthTop[idx])
                        elif df.grainFormPrimary[idx+1] == 'SH':
                            coldafterwarm = 'yes at' + str(df.depthTop[idx])
                        #grain Form Secondary
                        elif df.grainFormSecondary[idx+1] == 'FC':
                            warmaftercold = 'yes at' + str(df.depthTop[idx])                               
                        elif df.grainFormSecondary[idx+1] == 'FCxr':
                            warmaftercold = 'yes at' + str(df.depthTop[idx])
                        elif df.grainFormSecondary[idx+1] == 'DH':
                            warmaftercold = 'yes at' + str(df.depthTop[idx])  
                        elif df.grainFormSecondary[idx+1] == 'SH':  
                            coldafterwarm = 'yes at' + str(df.depthTop[idx])
                        else:
                            coldafterwarm = 'no'
                else:
                    pass
                    #print('This is the bottom  layer')
            else: 
                #not misinterpreted
                pass

        else:
            pass
            #print('this layer does not indicate a typical Gm4 pattern') 
                
    return(warmaftercold, coldafterwarm)
