# -*- coding: utf-8 -*-
"""
Created on Tue Jul 27 11:45:21 2021

@author: haroh
"""

from statistics_prepocessing import folderfoqk, preprocessing, df_id_qk, boxplots, freq_class
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as stats
from scipy.stats import chi2_contingency


#%% Built dataframe for parameters: 'slope','aspect', 'elevation', 'subregion' 
#   first for all Qualityclasses together then for each QK  

#define Variable
pathtofile =  r'C:\Users\haroh\Desktop\Gitlab\GM4\snowprofiles\output\download\all' 

#dataframe 'df_locRef_allqk' for all Qualityclasses
folder = []
#parse through dataframe which columns are: 'profile id' and 'QK'
for i in df_id_qk.identiy:
    string = 'snowprofile{}.xml'.format(i)
    folder.append(string)
#create empty dataframe and fill it with 
df_locRef_allqk = pd.DataFrame(columns = ['slope','aspect', 'elevation', 'subregion'], dtype=('float'))    
for name in folder:
    df  =  preprocessing(name, pathtofile)
    df_locRef_allqk = pd.concat([df, df_locRef_allqk],ignore_index=True)
    


##dataframe 'df_locRef_qk{}' for each Qualityclasses
def dataframelocref(QK):
    '''
    

    Parameters
    ----------
    QK : str
        Qualityclass of interest
        example: 3

    Returns
    -------
    df_locRef : pdDataFrame
        infromation about slope aspect subregion and elevation for each profile
        

    '''
    df_locRef = pd.DataFrame(columns = ['slope','aspect', 'elevation', 'subregion'], dtype=('float'))
    df_qk, folder = folderfoqk(str(QK))
    
    for name in folder:
        df  =  preprocessing(name, pathtofile)
        df_locRef = pd.concat([df, df_locRef],ignore_index=True)
        
    return df_locRef



df_locRef_qk1 = dataframelocref(1)
df_locRef_qk2 = dataframelocref(2)
df_locRef_qk3 = dataframelocref(3)
df_locRef_qk4 = dataframelocref(4)
df_locRef_qk5 = dataframelocref(5)
df_locRef_qk6 = dataframelocref(6)



#%%plot statistic for each qualityclass 

     
fig1,slopemedian1, elevationmedian1, slopemean1, elevationmean1 = boxplots(df_locRef_qk1,1)
fig2,slopemedian2, elevationmedian2, slopemean2, elevationmean2 = boxplots(df_locRef_qk2,2)
fig2,slopemedian3, elevationmedian3, slopemean3, elevationmean3 = boxplots(df_locRef_qk3,3)
fig4,slopemedian4, elevationmedian4, slopemean4, elevationmean4 = boxplots(df_locRef_qk4,4)
fig5,slopemedian5, elevationmedian5, slopemean5, elevationmean5 = boxplots(df_locRef_qk5,5)
fig6,slopemedian6, elevationmedian6, slopemean6, elevationmean6 = boxplots(df_locRef_qk6,6)
figall,slopemedianall, elevationmedianall, slopemeanall, elevationmeanall = boxplots(df_locRef_allqk,'all')

#df_stat_val = pd.DataFrame({'slope':slopeangle, 'aspect':aspectpos, 'elevation':elevation, 'subregion':subregion})

df_stat_val = pd.DataFrame({'QK': ['qk1',  'qk2',  'qk3',  'qk4',  'qk5',  'qk6', 'all'],
            'slopemedian': [slopemedian1, slopemedian2, slopemedian3, slopemedian4, slopemedian5, slopemedian6, slopemedianall], 
            'elevationmedian': [elevationmedian1,elevationmedian2,elevationmedian3,elevationmedian4,elevationmedian5,elevationmedian6,elevationmedianall],
            'slopemean' : [slopemean1, slopemean2, slopemean3, slopemean4, slopemean5, slopemean6, slopemeanall],
            'elevationmean':[elevationmean1, elevationmean2, elevationmean3, elevationmean4, elevationmean5, elevationmean6, elevationmeanall]})

print(df_stat_val)
#df_stat_val = df_stat_val.set_index('QK')
#%% dataframes pointing out the frequencies for each class for each
#parameter: slope aspect subregion and elevation
df_class_slope_allqk, df_class_aspect_allqk, df_class_elevation_allqk, df_class_subregion_allqk =  freq_class(df_locRef_allqk)
df_class_slopeqk1, df_class_aspectqk1, df_class_elevationqk1, df_class_subregionqk1 =  freq_class(df_locRef_qk1)
df_class_slopeqk2, df_class_aspectqk2, df_class_elevationqk2, df_class_subregionqk2 =  freq_class(df_locRef_qk2)
df_class_slopeqk3, df_class_aspectqk3, df_class_elevationqk3, df_class_subregionqk3 =  freq_class(df_locRef_qk3)
df_class_slopeqk4, df_class_aspectqk4, df_class_elevationqk4, df_class_subregionqk4 =  freq_class(df_locRef_qk4)
df_class_slopeqk5, df_class_aspectqk5, df_class_elevationqk5, df_class_subregionqk5 =  freq_class(df_locRef_qk5)
df_class_slopeqk6, df_class_aspectqk6, df_class_elevationqk6, df_class_subregionqk6 =  freq_class(df_locRef_qk6)


#%% creating contingenztable for each parameter:  slope aspect subregion and elevation


data_slope = [df_class_slope_allqk['bins_slope'], df_class_slopeqk1['slope'], df_class_slopeqk2['slope'], df_class_slopeqk3['slope'], df_class_slopeqk4['slope'],df_class_slopeqk5['slope'], df_class_slopeqk6['slope']]
headers_slope = ['bins_slope', 'qk1',  'qk2',  'qk3',  'qk4',  'qk5',  'qk6']
df_freqcl_slope = pd.concat(data_slope, axis=1, keys=headers_slope)
df_freqcl_slope = df_freqcl_slope.set_index('bins_slope')

data_aspect= [df_class_aspect_allqk['bins_aspect'], df_class_aspectqk1['aspect'], df_class_aspectqk3['aspect'], df_class_aspectqk3['aspect'], df_class_aspectqk4['aspect'],df_class_aspectqk5['aspect'], df_class_aspectqk6['aspect']]
headers_aspect = ['bins_aspect', 'qk1',  'qk2',  'qk3',  'qk4',  'qk5',  'qk6']
df_freqcl_aspect = pd.concat(data_aspect, axis=1, keys=headers_aspect)
df_freqcl_aspect = df_freqcl_aspect.set_index('bins_aspect')

data_elevation= [df_class_elevation_allqk['bins_elevation'], df_class_elevationqk1['elevation'], df_class_elevationqk2['elevation'], df_class_elevationqk3['elevation'], df_class_elevationqk4['elevation'],df_class_elevationqk5['elevation'], df_class_elevationqk6['elevation']]
headers_elevation = ['bins_elevation', 'qk1',  'qk2',  'qk3',  'qk4',  'qk5',  'qk6']
df_freqcl_elevation = pd.concat(data_elevation, axis=1, keys=headers_elevation)
df_freqcl_elevation = df_freqcl_elevation.set_index('bins_elevation')

data_subregion = [df_class_subregion_allqk['bins_subregion'], df_class_subregionqk1['subregion'], df_class_subregionqk2['subregion'], df_class_subregionqk3['subregion'], df_class_subregionqk4['subregion'],df_class_subregionqk5['subregion'], df_class_subregionqk6['subregion']]
headers_subregion = ['bins_subregion', 'qk1',  'qk2',  'qk3',  'qk4',  'qk5',  'qk6']
df_freqcl_subregion = pd.concat(data_subregion, axis=1, keys=headers_subregion)
df_freqcl_subregion = df_freqcl_subregion.set_index('bins_subregion')



#%% Homogenitätstest - H_0: sind die häufigkeiten der vordefinierten Klassen
#in alle QK im selben Verhätniss? 
# df_freqcl_slope = pd.concat([df_class_slopeqk1['bins_slope'], df_freqcl_slope['qk1'], df_freqcl_slope['qk3']])
# df_freqcl_slope = df_freqcl_slope.drop([0])
# df_freqcl_slope = df_freqcl_slope.drop([2])
# df_freqcl_slope = df_freqcl_slope.set_index('bins_slope')

print(stats.chi2_contingency(df_freqcl_slope))

print('chisqu_slope'+ str(stats.chi2_contingency(df_freqcl_slope)))
print('chisqu_elevation'+ str(stats.chi2_contingency(df_freqcl_elevation)))
print('chisqu_aspect'+ str(stats.chi2_contingency(df_freqcl_aspect)))




#%% unabhängigkeistest - H_0: Die Hangneigung beeinflusst die Anzahl der
#Profile in den einzelnen QK nicht. 
df_freqcl_slope_trans = df_freqcl_slope.T
df_freqcl_elevation_trans = df_freqcl_elevation.T
df_freqcl_aspect_trans = df_freqcl_aspect.T
print('chisqu_slope_indep' + str(stats.chi2_contingency(df_freqcl_slope_trans)))
print('chisqu_elevation_indep' + str(stats.chi2_contingency(df_freqcl_elevation_trans)))
print('chisqu_aspect_indep'+ str(stats.chi2_contingency(df_freqcl_aspect_trans)))
print(df_stat_val)


