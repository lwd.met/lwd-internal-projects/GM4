# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 16:42:20 2021

@author: haroh
"""
import requests
import os.path
import numpy as np
import xml.etree.ElementTree as et
import pandas as pd
import matplotlib.pyplot as plt


#%% Deining Variables 
#path to file which shows profiliD with associated QK
path = r'C:\Users\haroh\OneDrive\Arbeit LWD-DESKTOP-LP46IDF\Masterarbeit\Daten\Profiles QK discarded\id_QK.csv'
#dataframe columns = 'profile id' and 'QK'
df_id_qk = pd.read_csv(path,header=0,sep=';', low_memory=False)
#path to profiles of every season (201718 -202021)
pathtofile =  r'C:\Users\haroh\Desktop\Gitlab\GM4\snowprofiles\output\download\all'


#%%
def folderfoqk(qk):
    '''
    

    Parameters
    ----------
    qk : string
        Qualityclass of interest

    Returns
    -------
    df_idqbqk : pd.DataFrame
        Dataframe including id and QK of chosen QK 
    folder : list
       folder where python can find the profiles and further creat a list 
       cointaining all profiles of choosen QK

    '''
    
    df_id_gb =  df_id_qk.groupby('QK')
    df_idqbqk =  df_id_gb.get_group(qk)
    
    folder = []
    for i in df_idqbqk.identiy:
        string = 'snowprofile{}.xml'.format(i)
        folder.append(string)
    
    return df_idqbqk, folder


def preprocessing(filename, pathtofile):
    '''
    parase xml snowprofile trace back 2 DataFrame with with values of interest

    Parameters
    ----------
    filename : str
        name of xml file of snowprofile
    pathtofile : str
        directorypath to snowprofile

    Returns
    -------
    df_md : pd.DataFrame
        meta data of the snowprofile including slopenagle aspectpos elevation 
        and subregion 
        
    '''
    slopeangle =[]
    aspectpos = []
    elevation = []
    subregion = []

    #define path where profiles are stored
    pathtoparase = os.path.join(pathtofile, filename)
    
    #parse the xml object
    tree = et.parse(pathtoparase)
    #dumping entire tree to variable xroot
    xroot = tree.getroot()
    
        
    #pase the xml file 
    for child in xroot:
        xroot1 = et.Element('root1')
        xroot1 = (child)
        for grandchild in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}locRef'):
            xroot2 = et.Element('root2')
            xroot2 = (grandchild)
            for grandgrandchild1 in xroot2.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}SlopeAnglePosition'):
                xroot2_1 = et.Element('root2_1')
                xroot2_1 = (grandgrandchild1)
                for angle in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                    slopeangle.append(angle.text)
                    
            for grandgrandchild2 in xroot2.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}AspectPosition'):
                xroot2_2 = et.Element('root2_2')
                xroot2_2 = (grandgrandchild2)
                for aspect in xroot2_2.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                    aspectpos.append(aspect.text) 
                    
            for grandgrandchild3 in xroot2.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}ElevationPosition'):
                xroot2_3 = et.Element('root2_3')
                xroot2_3 = (grandgrandchild3)
                for elev in xroot2_3.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                    elevation.append(elev.text)
                    
            for grandgrandchild4 in xroot2.iter('{https://www.lawis.at/snowprofile/1.0}location'):
                xroot2_4 = et.Element('root2_4')
                xroot2_4 = (grandgrandchild4)
                for subreg in xroot2_4.iter('{https://www.lawis.at/snowprofile/1.0}subregion'):
                    subregion.append(subreg.text)
                    
                           
       
    #create a dataframe including the metadata slopenagle aspectpos elevation 
    #and subregion 
    df_md = pd.DataFrame({'slope':slopeangle, 'aspect':aspectpos, 'elevation':elevation, 'subregion':subregion})
    
    #change strings to values 
    #aspect: N = 0 and S=180...
    df_md.aspect = df_md.aspect.replace(to_replace =["N"], value = 360)
    df_md.aspect = df_md.aspect.replace(to_replace =["NE"], value = 45)
    df_md.aspect = df_md.aspect.replace(to_replace =["E"], value = 90)
    df_md.aspect = df_md.aspect.replace(to_replace =["SE"], value = 135)
    df_md.aspect = df_md.aspect.replace(to_replace =["S"], value = 180)
    df_md.aspect = df_md.aspect.replace(to_replace =["SW"], value = 225)
    df_md.aspect = df_md.aspect.replace(to_replace =["W"], value = 270)
    df_md.aspect = df_md.aspect.replace(to_replace =["NW"], value = 315)
    
    #subregions: numbers correspond to the official numbering 
    df_md.subregion = df_md.subregion.replace(to_replace =["Allgäuer Alpen"], value = 1)
    df_md.subregion = df_md.subregion.replace(to_replace =["Östl. Lechtaler A. - Ammergauer A."], value = 2)
    df_md.subregion = df_md.subregion.replace(to_replace =["Mieminger Gebirge"], value = 3)
    df_md.subregion = df_md.subregion.replace(to_replace =["Karwendel"], value = 4)
    df_md.subregion = df_md.subregion.replace(to_replace =["Brandenberger Alpen"], value = 5)
    df_md.subregion = df_md.subregion.replace(to_replace =["Wilder Kaiser - Waidringer Alpen"], value = 6)
    df_md.subregion = df_md.subregion.replace(to_replace =["Westl. Lechtaler Alpen"], value = 7)
    df_md.subregion = df_md.subregion.replace(to_replace =["Zentrale Lechtaler Alpen"], value = 8)
    df_md.subregion = df_md.subregion.replace(to_replace =["Grieskogelgruppe"], value = 9)
    df_md.subregion = df_md.subregion.replace(to_replace =["Westl. Verwallgruppe"], value = 10)
    df_md.subregion = df_md.subregion.replace(to_replace =["Östl. Verwallgruppe"], value = 11)
    df_md.subregion = df_md.subregion.replace(to_replace =["Silvretta"], value = 12)
    df_md.subregion = df_md.subregion.replace(to_replace =["Samnaungruppe"], value = 13)
    df_md.subregion = df_md.subregion.replace(to_replace =["Nördl. Ötztaler- und Stubaier Alpe"], value = 14)
    df_md.subregion = df_md.subregion.replace(to_replace =["Westl. Tuxer Alpen"], value = 15)    
    df_md.subregion = df_md.subregion.replace(to_replace =["Östl. Tuxer Alpen"], value = 16)
    df_md.subregion = df_md.subregion.replace(to_replace =["Westl. Kitzbüheler Alpen"], value = 17)
    df_md.subregion = df_md.subregion.replace(to_replace =["Östl. Kitzbüheler Alpen"], value = 18)
    df_md.subregion = df_md.subregion.replace(to_replace =["Glockturmgruppe"], value = 19)
    df_md.subregion = df_md.subregion.replace(to_replace =["Weißkugelgruppe"], value = 20)       
    df_md.subregion = df_md.subregion.replace(to_replace =["Gurgler Gruppe"], value = 21) 
    df_md.subregion = df_md.subregion.replace(to_replace =["Zentrale Stubaier Alpen"], value = 22) 
    df_md.subregion = df_md.subregion.replace(to_replace =["Nördl. Zillertaler Alpen"], value = 23) 
    df_md.subregion = df_md.subregion.replace(to_replace =["Venedigergruppe"], value = 24) 
    df_md.subregion = df_md.subregion.replace(to_replace =["Östl. Rieserfernergruppe"], value = 25) 
    df_md.subregion = df_md.subregion.replace(to_replace =["Glocknergruppe"], value = 26) 
    df_md.subregion = df_md.subregion.replace(to_replace =["Östl. Deferegger Alpen"], value = 27) 
    df_md.subregion = df_md.subregion.replace(to_replace =["Schobergruppe"], value = 28) 
    df_md.subregion = df_md.subregion.replace(to_replace =["Lienzer Dolomiten"], value = 29) 
    
    
    df_md.slope = pd.to_numeric(df_md.slope)
    df_md.elevation = pd.to_numeric(df_md.elevation)
    df_md = df_md.astype('float')
    
    
    
    return df_md

#%%         
                
def boxplots(df_QK, qk):
    '''

    Parameters
    ----------
    df_QK : pd DataFrame
        dataframe including all parameters for selected QK
        example: df_locRef_qk1
    qk : str
        qualityclass as a str for plotting 
        example "1"

    Returns
    -------
    fig : AxesSubplot
        plot with statistics of selected QK
        
    prints informations about statistics of the QK 

    '''
    
    fig, ax = plt.subplots(2, 3, figsize=(11, 5))
    fig.suptitle('Qualityclass ' + str(qk))
    qk = str(qk)
    
    bins_aspect = [0,45,90,135,180,225,270,315]
    bins_subregion = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29]
    bins_slope = [0,5,10,15,20,25,30,35,40,45,50]
    bins_elevation = [1000, 1250, 1500, 1750,2000, 2250, 2500, 2750, 3000, 3250, 3500, 3750]
    
    
    
    df_QK.boxplot('slope', ax=ax[0,0])
    df_QK.boxplot('elevation', ax=ax[0,1])

    
    df_QK.aspect.plot.hist(bins= bins_aspect, width=43,  align='left', ax=ax[0,2])
    df_QK.subregion .plot.hist(bins= bins_subregion, width=0.8,  align='left', ax=ax[1,2])
    df_QK.slope.plot.hist(bins = bins_slope, width=3,  align='left', ax=ax[1,0])
    df_QK.elevation.plot.hist(bins = bins_elevation, width=225,  align='left', ax=ax[1,1])


    
    # Hide the right and top spines
    ax[0,0].spines['right'].set_visible(False)
    ax[0,0].spines['top'].set_visible(False)
    ax[0,0].grid(False)
    
    ax[0,1].spines['right'].set_visible(False)
    ax[0,1].spines['top'].set_visible(False)
    ax[0,1].grid(False)
    
    
    ax[0,2].spines['right'].set_visible(False)
    ax[0,2].spines['top'].set_visible(False)
    ax[0,2].set_xticks(bins_aspect)
    ax[0,2].set_ylabel('frequency of aspect')
    
    ax[1,0].spines['right'].set_visible(False)
    ax[1,0].spines['top'].set_visible(False)
    ax[1,0].set_ylabel('frequency of slope')
    
    ax[1,1].spines['right'].set_visible(False)
    ax[1,1].spines['top'].set_visible(False)
    ax[1,1].set_ylabel('frequency of elevation')
    
    ax[1,2].spines['right'].set_visible(False)
    ax[1,2].spines['top'].set_visible(False)
    ax[1,2].grid(False)
    ax[1,2].set_ylabel('frequency of subregion')
    
    fig.tight_layout()
    
    
    # print('statistical values of QK ' + str(qk) + ' \n')
    # print('Slope Median of QK' + str(qk) + ' ' + str(df_QK.slope.median()))
    # print('elevation Median of QK' + str(qk) + ' ' + str(df_QK.elevation.median()) + ' \n')
    
    # print('Slope mean of QK' + str(qk) + ' ' + str(round(df_QK.slope.mean(),2)))
    # print('elevation mean of QK' + str(qk) + ' ' + str(round(df_QK.elevation.mean(),2))+' \n')
    
    slopemedian = df_QK.slope.median()
    elevationmedian = df_QK.elevation.median()

    slopemean = round(df_QK.slope.mean(),2)
    elevationmean = round(df_QK.elevation.mean(),2)
          
    return fig, slopemedian, elevationmedian, slopemean, elevationmean
  

#%%classes for unabhängigkeitstests



def freq_class(df):
    '''
    

    Parameters
    ----------
    df : pd.DataFrame
        Dataframe of qualityclass
        example: df_locRef_qk1 - inlcuedes all metadata for each profile in one qualitiyclass

    Returns
    -------
    df_class_slope : pd.DataFrame
        dataframe of classes and corresponding frequencies for slope 
    df_class_aspect : pd.DataFrame
        dataframe  of classes and corresponding frequencies for aspect
    df_class_elevation : pd.DataFrame
       dataframe  of classes and corresponding frequencies for elevations
    df_class_subregion : pd.DataFrame
        dataframe  of classes and corresponding frequencies for subregion

    '''

    bins_slope = np.linspace(0,50,11)
    df['bins_slope'] = pd.cut(df['slope'], bins=bins_slope, include_lowest=True)
    old_slope= df.groupby(['bins_slope'],as_index=False).count().astype('object')
    #old_slope= df.groupby(['bins_slope'],as_index=False).count().astype('object')
    df_class_slope = old_slope.filter(['bins_slope','slope'])

    #df_class_slope = pd.DataFrame(zip(old_slope.bins_slope, old_slope.slope))

    bins_aspect = np.linspace(0,360,9)
    df['bins_aspect'] = pd.cut(df['aspect'], bins=bins_aspect, include_lowest=True)
    old_aspect= df.groupby(['bins_aspect'],as_index=False).count().astype('object')
    df_class_aspect = old_aspect.filter(['bins_aspect','aspect'])
    
    
    bins_elevation= np.linspace(1000,4000,7)
    df['bins_elevation'] = pd.cut(df['elevation'], bins=bins_elevation, include_lowest=True)
    old_elev= df.groupby(['bins_elevation'],as_index=False).count().astype('object')
    df_class_elevation= old_elev.filter(['bins_elevation','elevation'])
    
    
    bins_subregion= np.linspace(1,29,29)
    df['bins_subregion'] = pd.cut(df['subregion'], bins=bins_subregion, include_lowest=True)
    old_subr= df.groupby(['bins_subregion'],as_index=False).count().astype('object')
    df_class_subregion = old_subr.filter(['bins_subregion','subregion'])
    
    return df_class_slope, df_class_aspect, df_class_elevation, df_class_subregion




    
    