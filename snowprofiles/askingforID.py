# -*- coding: utf-8 -*-
"""
Created on Thu Jul  1 13:48:02 2021

@author: haroh
"""

import requests
import os.path
import numpy as np
import xml.etree.ElementTree as et
import pandas as pd



#directorypath snowporfiles
season = '201718'
path_basic_xml = r'C:\Users\haroh\Desktop\Gitlab\GM4\snowprofiles\output\download'
path_profile = os.path.join(path_basic_xml, season)

#folder where python can find the profiles and further creat a list 
#cointaining all profiles of choosen season 
folder = os.listdir(path_profile)
#%%
date = []
identiy = []
region = []
location = []
elevation = []
aspect = []
slopeangle =  []
geoinfo =  []
name = []
long = []
lat = []
    
for file in folder:
    identiy.append(file[11:-4])
    #define path where profiles are stored
    pathtoparase = os.path.join(path_profile, file)
    
    #parse the xml object
    tree = et.parse(pathtoparase)
    #dumping entire tree to variable xroot
    xroot = tree.getroot()   
       
 
    for child in xroot:
        xroot1 = et.Element('root1')
        xroot1 = (child)
        for grandchild in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}timeRef'):
            xroot2_0 = et.Element('root2_1')
            xroot2_0 = (grandchild)
            for time in xroot2_0.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}timePosition'):
                date.append(time.text)
                
        for grandchild1 in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}locRef'):
                    xroot2_1 = et.Element('root2_1')
                    xroot2_1 = (grandchild1)                        
                    for locinfo in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}validElevation'):
                        xroot3_0 = et.Element('root3_0')
                        xroot3_0 = (locinfo)
                        for locinfoelevation in xroot3_0.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                            elevation.append(locinfoelevation.text)
                            
                    for locinfo1 in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}validAspect'):
                        xroot3_1 = et.Element('root3_01')
                        xroot3_1 = (locinfo1)
                        for locinfoaspect in xroot3_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                            aspect.append(locinfoaspect.text)  
                            
                    for locinfo2 in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}validSlopeAngle'):
                        xroot3_2 = et.Element('root3_02')
                        xroot3_2 = (locinfo2)
                        for locinfoslopeangle in xroot3_2.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                            slopeangle.append(locinfoslopeangle.text)   
                            
                    for locinfo3 in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}pointLocation'):
                        xroot3_3 = et.Element('root3_03')
                        xroot3_3 = (locinfo3)
                        for locinfolong in xroot3_3.iter('{http://www.opengis.net/gml}pos'):
                            geoinfo.append(locinfolong.text)
                            long.append(locinfolong.text[0:7]) 
                            lat.append(locinfolong.text[-8:])                
 
    
list_tuple = list(zip(identiy, date, aspect, elevation, slopeangle, long, lat))            
df_identiy = pd.DataFrame(list_tuple, columns=['identiy', 'Date', 'Aspect', 'Elevation', 'SlopeAngle', 'long', 'lat'])    
       
#%%   

date1=[]
count =0
for i in date:
    if i not in date1:
        date1.append(i)
        count +=1
    else:
        print(i,end=' ')



#%%

path_holytable = r'C:\Users\haroh\Desktop\Gitlab\GM4\snowprofiles\input'
df_holytable = pd.read_csv('input\snowprofiles_discarded2_201718.csv',sep=";")


#%%            
df_new = pd.merge(df_identiy, df_holytable, on=["Date", "lat"])
print(len(df_new) == len(df_holytable))

        
df_new.to_csv(r"C:\Users\haroh\Desktop\Gitlab\GM4\snowprofiles\output\id\201718id.csv")
        
        