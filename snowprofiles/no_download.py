# -*- coding: utf-8 -*-
"""
Created on Mon Apr 19 14:25:59 2021

@author: haroh
"""
import requests
import os.path
import numpy as np
import xml.etree.ElementTree as et
import pandas as pd


profil_id = '15389'

url_layer = 'https://www.lawis.at/lawis_api/public/profile/{}?format=xml'.format(profil_id)
data_layers = requests.get(url_layer)
#%%
tree = et.fromstring(requests.get('http://synd.cricbuzz.com/j2me/1.0/livematches.xml').text)
#parse the xml object
#tree = et.parse(pathtoparase)
#dumping entire tree to variable xroot
xroot = tree.getroot()
#%%

tree = et.parse(pathtoparase)
#dumping entire tree to variable xroot
xroot = tree.getroot()

#creates empty lists for layers
depthTop = []
thickness = []
grainFormPrimary = []
grainFormSecondary = []
hardness = []
date = []
#creates empty lists for meta data
region = []
location = []
elevation = []
aspect = []
slopeangle =  []
geoinfo =  []
name = []
long = []
lat = []

for child in xroot:
    xroot1 = et.Element('root1')
    xroot1 = (child)
    for grandchild in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}srcRef'):
        xroot2_0 = et.Element('root2_0')
        xroot2_0 = (grandchild)
        for elem in xroot2_0.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}name'):
            name.append(elem.text)
    for grandchild1 in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}locRef'):
        xroot2_1 = et.Element('root2_1')
        xroot2_1 = (grandchild1)
        for elem2 in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}name'):
            location.append(elem2.text)
            
        for locinforegion in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}region'):
            region.append(locinforegion.text)  
            
        for locinfo in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}validElevation'):
            xroot3_0 = et.Element('root3_0')
            xroot3_0 = (locinfo)
            for locinfoelevation in xroot3_0.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                elevation.append(locinfoelevation.text)
        for locinfo1 in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}validAspect'):
            xroot3_1 = et.Element('root3_01')
            xroot3_1 = (locinfo1)
            for locinfoaspect in xroot3_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                aspect.append(locinfoaspect.text)                
        for locinfo2 in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}validSlopeAngle'):
            xroot3_2 = et.Element('root3_02')
            xroot3_2 = (locinfo2)
            for locinfoslopeangle in xroot3_2.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}position'):
                slopeangle.append(locinfoslopeangle.text)                
        for locinfo3 in xroot2_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}pointLocation'):
            xroot3_3 = et.Element('root3_03')
            xroot3_3 = (locinfo3)
            for locinfolong in xroot3_3.iter('{http://www.opengis.net/gml}pos'):
                geoinfo.append(locinfolong.text)
                long.append(locinfolong.text[0:7]) 
                lat.append(locinfolong.text[-8:])         
         

        
    for grandchild2 in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}snowProfileResultsOf'):
        xroot2 = et.Element('root2') 
        xroot2 = grandchild2
        for measurment in xroot2.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}SnowProfileMeasurements'):
            xroot3 = et.Element('root3') 
            xroot3 = (measurment)
            for layer in xroot3.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}stratProfile'):
                xroot4 = et.Element('root4') 
                xroot4 = (layer)                
                for val_depth in xroot4.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}depthTop'):
                    depthTop.append(val_depth.text)
                for val_thickness in xroot4.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}thickness'):
                    thickness.append(val_thickness.text)
                for form in xroot4.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}grainFormPrimary'):
                    grainFormPrimary.append(form.text)
                for form2 in xroot4.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}grainFormSecondary'):
                    grainFormSecondary.append(form2.text)
                for val_hardness in xroot4.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}hardness'):
                    hardness.append(val_hardness.text)

    for grandchild3 in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}timeRef'):
        xroot2_3 = et.Element('root2_03')
        xroot2_3 = (grandchild3)
        for time in xroot2_3.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}timePosition'):
            date.append(time.text)
    
    for grandchild4 in xroot1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}metaData'):
        xroot4_1 = et.Element('root4_01')
        xroot4_1 = (grandchild4)
        for data in xroot4_1.iter('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}comment'):
            comment = (data.text)
            
            
#design DataFrame to store values of interest                     
df_md = pd.DataFrame(columns=['Date', 'Name', 'location', 'long' , 'lat', 
                            'Elevation', 'Aspect','SlopeAngle'])

df = pd.DataFrame(columns=['depthTop','thickness','grainFormPrimary',
                           'grainFormSecondary','hardness'])

#select only snowprofiles recorded Tirol
if region[0] == 'Tirol':
  
    #dataframe_metadata
    df_md = pd.DataFrame({'Date':date, 'Name':name, 'location':location, 'long': long , 
                      'lat': lat, 'Elevation':elevation, 'Aspect':aspect,
                      'SlopeAngle': slopeangle})
            
    #dataframe_snowprofile
    df = pd.DataFrame({'depthTop': depthTop,'thickness':thickness,
                   'grainFormPrimary':grainFormPrimary,
                   'grainFormSecondary':grainFormSecondary,
                   'hardness':hardness})
else:
    pass


# Convert str of hardness to float
# 1 = F- = Faust
# 1-2 = F+
# 2 = 4F = 4Finger
# 3 = 1F = 1Finger
# 4-4 = 1F+
# 4 = P = Bleistift
# 4-5 = P+
# 5 = K = Knife 
# 5-6 = M- 
# 6 = I = Ice


for idx, layer_str in enumerate(df.hardness):
    if layer_str == 'F-':
        df.hardness[idx] = 1
    elif layer_str == 'F+':
        df.hardness[idx] = 1.5
    elif layer_str == '4F':
        df.hardness[idx] = 2
    elif layer_str == '4F+':
        df.hardness[idx] = 2.5
    elif layer_str == '1F':
        df.hardness[idx] = 3
    elif layer_str == '1F+':
        df.hardness[idx] = 3.5
    elif layer_str == 'P':
        df.hardness[idx] = 4
    elif layer_str == 'P+':
        df.hardness[idx] = 4.5
    elif layer_str == 'K':
        df.hardness[idx] = 5  
    elif layer_str == 'K+':
        df.hardness[idx] = 5.5 
    elif layer_str == 'I':
        df.hardness[idx] = 6 





#%% 
