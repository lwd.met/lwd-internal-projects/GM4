# -*- coding: utf-8 -*-
"""

This Skript uses input data season and hardness to parse LAWIS and returns a 
1. a folder containg all snowprofiles of the chosen season and hardness 
2. a folder containing a .png file of all snow profiles showing a dp4 pattern 
   of the chosen season and hardness 
3. a *.csv file with a table containing all dp4 profiles for chosen season and
   hardness 
   
input data:
-----------
download_files: str
                when running the script you will be asked wheater you want to 
                download all *.xml files or not. 
                Enter "YES" for downloading and "NO" if files are already 
                downloaded'

season :        str
                when running the script you will be asked which season you want
                to analyze. Please enter the year of the corresponding season. 
                For example 201920 
        
hardness :      str
                when running the script you will be asked which hardness level 
                you want to analyze. 
                Please enter 1 or 1.5 for analyse "hardness" = 1 or "hardness" <2. 
        
The following line lists the index of snowprofiles for already analysed seasons
Those indexes need to be looked up in LAWIS maually. 

6486 = 30.10.2017 
8306 = 30.04.2018

8457 = 03.11.2018
10630 = 02.05.2019

10674 = 31.10.2019
12685 = 29.05.2020

12698 = 08.10.2020
15284 = 06.04.2021




Created on Tue Apr  6 10:00:31 2021

@author: haroh
"""

from preprocoessing_xml_caaml import DP4, preprocessing, download
import os
import pandas as pd
import numpy as np
import requests



#%%
season = input('Enter Season you like to analyse - example 202021: ')
hardness = float(input('Decide if you want to analyse "hardness" = 1 or "hardness" <2 - Therfore enter 1 or 1.5: '))
download_files = input('Decide if you want to download all xml files or not.\n Enter "YES" for downloading and "NO" if files are already downloaded: ')

#directorypath for saving the snowporfiles
path_basic_xml = r'C:\Users\haroh\Desktop\Gitlab\GM4\snowprofiles\output\download'
path_profile = os.path.join(path_basic_xml, season)
if not os.path.exists(path_profile):
    os.makedirs(path_profile)
    
#directorypath for saving png
path_basic_png = r'C:\Users\haroh\Desktop\Gitlab\GM4\snowprofiles\output\png'
path_png = os.path.join(path_basic_png, season)
if not os.path.exists(path_png):
    os.makedirs(path_png)

#str for saving
degreeofhardness = 'Hardnessgrade_{}'.format(hardness) 

#%%

#download files directly from LAWIS
if download_files == 'YES':
    download(8457, 10630, path_profile)
else:
    pass

#folder where python can find the profiles and further creat a list 
#cointaining all profiles of choosen season 
folder = os.listdir(path_profile)

#create DataFrame for saving DP4 profiles 
df_save = pd.DataFrame(columns=['Date', 'Name', 'location', 'long' , 'lat', 
                                'Elevation', 'Aspect','SlopeAngle', 
                                'DP4 info'])

#loop through all .xml files generate a pd.DataFrame with all all dp4 profiles
#as well as a folder containing png files from of all snow profiles showing
#a dp4 pattern 
for file in folder:
    try:
        df, df_md, comments  = preprocessing(file, path_profile)

        warmaftercold, coldafterwarm  = DP4(df, hardness)

        #ask if profiles shows dp4 pattern
        if str(warmaftercold[0:3]) == 'yes':
            df_md['DP4 info'] = 'warmaftercold'
            #fill DataFrame_save if profile shows a DP4 pattern
            df_save_new = pd.concat([df_save, df_md])
            df_save = df_save_new
            
            #save png if profile shows a DP4 pattern
            profile = file.split('.')
            profile = profile[0][11::]
            #print(profile)
            
            url = 'https://www.lawis.at/lawis_api/v2_2/files/profiles/snowprofile_{}.png?v=1'.format(profile)
            res = requests.get(url, allow_redirects=True)
            #res.raise_for_status()
            file = 'snowprofile{}.png'.format(profile)
            path_hardness = os.path.join(path_png, degreeofhardness)
            if not os.path.exists(path_hardness):
                os.makedirs(path_hardness)
            path_new = os.path.join(path_hardness, file)    
            open(str(path_new), 'wb').write(res.content)
            
        elif str(coldafterwarm[0:3]) == 'yes':
            df_md['DP4 info'] = 'coldafterwarm'
            #fill DataFrame_save if profile shows a DP4 pattern
            df_save_new = pd.concat([df_save, df_md])
            df_save = df_save_new
            
            #save png if profile shows a DP4 pattern
            profile = file.split('.')
            profile = profile[0][11::]
            #print(profile)
            
            url = 'https://www.lawis.at/lawis_api/v2_2/files/profiles/snowprofile_{}.png?v=1'.format(profile)
            res = requests.get(url, allow_redirects=True)
            #res.raise_for_status()
            file = 'snowprofile{}.png'.format(profile)
            path_hardness = os.path.join(path_png, degreeofhardness)
            if not os.path.exists(path_hardness):
                os.makedirs(path_hardness)
            path_new = os.path.join(path_hardness, file)    
            open(str(path_new), 'wb').write(res.content)
            
        elif len(comments) >=5:
            if 'GM4' in comments or 'gm4' in comments or 'GM 4' in comments or 'gm 4' in comments :
                df_md['DP4 info'] = 'comment'
                #fill DataFrame_save if profile shows a DP4 pattern
                df_save_new = pd.concat([df_save, df_md])
                df_save = df_save_new
                
                #save png if profile shows a DP4 pattern
                profile = file.split('.')
                profile = profile[0][11::]
                #print(profile)
                
                url = 'https://www.lawis.at/lawis_api/v2_2/files/profiles/snowprofile_{}.png?v=1'.format(profile)
                res = requests.get(url, allow_redirects=True)
                #res.raise_for_status()
                file = 'snowprofile{}.png'.format(profile)
                path_hardness = os.path.join(path_png, degreeofhardness)
                if not os.path.exists(path_hardness):
                    os.makedirs(path_hardness)
                path_new = os.path.join(path_hardness, file)    
                open(str(path_new), 'wb').write(res.content)    
            
        
    except:
        pass
        #print(file)
        

#save pd.DataFrame containing all dp4 profiles to e csv table 
df_save.to_csv(r'C:\Users\haroh\Desktop\Gitlab\GM4\output\csv\snowprofiles_{}_{}.csv'.format(season, degreeofhardness), index = False)   
        

#shortcuts for Snow crystal shape:
#Precip particles = Neuschnee = PP
#decomp/fragm = flizig = df
#Rounded Grains = kleine Runde = RG
#kantig = faceted cystals = FC
#Depth hoar= Tiefenreif = DH
#Surface hoar = Oberflächenreif = SH
#Meltforms = Schmelzform = MF
#kantig abgerundet = faceted rounded FCxr
#Graupel = Graupel = PPgp
#Schmelzkruste = Melt freez crust = MFcr
   
