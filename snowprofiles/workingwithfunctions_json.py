# -*- coding: utf-8 -*-
"""

This Skript uses input data season and hardness to parse LAWIS and returns a 
1. a folder containg all snowprofiles of the chosen season and hardness 
2. a folder containing a .png file of all snow profiles showing a dp4 pattern 
   of the chosen season and hardness 
3. a *.csv file with a table containing all dp4 profiles for chosen season and
   hardness 
   
input data:
-----------
download_files: str
                when running the script you will be asked wheater you want to 
                download all *.xml files or not. 
                Enter "YES" for downloading and "NO" if files are already 
                downloaded'

season :        str
                when running the script you will be asked which season you want
                to analyze. Please enter the year of the corresponding season. 
                For example 201920 
        
hardness :      str
                when running the script you will be asked which hardness level 
                you want to analyze. 
                Please enter 1 or 1.5 for analyse "hardness" = 1 or "hardness" <2. 
        
The following line lists the index of snowprofiles for already analysed seasons
Those indexes need to be looked up in LAWIS maually. 

6486 = 30.10.2017 
8306 = 30.04.2018

10674 = 31.10.2019
12685 = 29.05.2020

12698 = 08.10.2020
15284 = 06.04.2021


Created on Tue Apr  6 10:00:31 2021

@author: haroh
"""

from preprocoessing_json import DP4, preprocoessing 
import os
import pandas as pd
import numpy as np
import requests
import datetime
from datetime import datetime
    


#%% define input variables

start = input('Enter enter the preferred start date in the format "YYYY-MM-DD": ')
stop  = input('Enter enter the preferred end date in the format "YYYY-MM-DD": ') 
hardness = float(input('Decide if you want to analyse "hardness" = 1 or "hardness" <2 - Therfore enter 1 or 1.5: '))


#%% prework for saving

season = start[0:4] + stop[0:2]

    
#directorypath for saving png
path_basic_png = r'C:\Users\haroh\Desktop\Gitlab\GM4\snowprofiles\png'
path_png = os.path.join(path_basic_png, season)
if not os.path.exists(path_png):
    os.makedirs(path_png)

#str for saving
degreeofhardness = 'Hardnessgrade_{}'.format(hardness) 

#create DataFrame for saving DP4 profiles 
df_save = pd.DataFrame(columns=['identity' ,'Date','Name', 'region', 
                                'subregion', 'location','long' , 'lat',
                                'Elevation', 'Aspect', 'SlopeAngle', 
                                'DP4 info'])

#%%
#loop through all .xml files generate a pd.DataFrame with all all dp4 profiles
#as well as a folder containing png files from of all snow profiles showing
#a dp4 pattern 

beginn = datetime.strptime(start, "%Y-%m-%d")
end = datetime.strptime(stop, "%Y-%m-%d")

data = requests.get("https://lawis.at/lawis_api/v2_2/profile/" ).json()
for i in range(0, len(data)):
    if data[i]['region_id'] == 1: 
        if beginn <= datetime.strptime(data[i]['datum'], "%Y-%m-%d %H:%M:%S") <= end:
            profile_id = (data[i]['profil_id'])
            try:
                df_md, df  = preprocoessing(profile_id)
                warmaftercold, coldafterwarm  = DP4(df, hardness)                   
                #ask if profiles shows dp4 pattern
                if str(warmaftercold[0:3]) == 'yes':
                    df_md['DP4 info'] = 'warmaftercold'
                    #fill DataFrame_save if profile shows a DP4 pattern
                    df_save_new = pd.concat([df_save, df_md])
                    df_save = df_save_new
                    
                    
                    url = 'https://www.lawis.at/lawis_api/v2_2/files/profiles/snowprofile_{}.png?v=1'.format(profile_id)
                    res = requests.get(url, allow_redirects=True)
                    #res.raise_for_status()
                    file = 'snowprofile{}.png'.format(profile_id)
                    path_hardness = os.path.join(path_png, degreeofhardness)
                    if not os.path.exists(path_hardness):
                        os.makedirs(path_hardness)
                    path_new = os.path.join(path_hardness, file)    
                    open(str(path_new), 'wb').write(res.content)
                elif str(coldafterwarm[0:3]) == 'yes':
                    df_md['DP4 info'] = 'coldafterwarm'
                    #fill DataFrame_save if profile shows a DP4 pattern
                    df_save_new = pd.concat([df_save, df_md])
                    df_save = df_save_new
                    
                    #save png if profile shows a DP4 pattern
                    
                    url = 'https://www.lawis.at/lawis_api/v2_2/files/profiles/snowprofile_{}.png?v=1'.format(profile_id)
                    res = requests.get(url, allow_redirects=True)
                    #res.raise_for_status()
                    file = 'snowprofile{}.png'.format(profile_id)
                    path_hardness = os.path.join(path_png, degreeofhardness)
                    if not os.path.exists(path_hardness):
                        os.makedirs(path_hardness)
                    path_new = os.path.join(path_hardness, file)    
                    open(str(path_new), 'wb').write(res.content)
            except:
                print('Not able to parse profile with id: ' + str(profile_id))
                pass
        

#save pd.DataFrame containing all dp4 profiles to e csv table 
df_save.to_csv(r'C:\Users\haroh\Desktop\Gitlab\GM4\snowprofiles_{}_{}.csv'.format(season, degreeofhardness), index = False)   
        
   
#shortcuts for Snow crystal shape:
#Precip particles = Neuschnee = PP
#decomp/fragm = flizig = df
#Rounded Grains = kleine Runde = RG
#kantig = faceted cystals = FC
#Depth hoar= Tiefenreif = DH
#Surface hoar = Oberflächenreif = SH
#Meltforms = Schmelzform = MF
#kantig abgerundet = faceted rounded FCxr
#Graupel = Graupel = PPgp
#Schmelzkruste = Melt freez crust = MFcr
