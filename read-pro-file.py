# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 09:22:00 2021
Reads .pro file. <-- Output from snowpack

@author: haroh
"""
# Standard library imports
import pandas as pd
import os


#%% Object to read .pro file
listlines = list()
# Rows of header. These rows are not read later on.
skip_header = 47

#%% Read file with manually set path

# Example file
tail = 'PESE21.pro' 
path_input = 'input'
#path_input = 'C:/Users/hadis/Documents/LWD/SNOWPACK/Data/SMET_simulated/SMET_simulated'
#path_input = 'C:\\Users\\hadis\\Documents\\LWD\\SNOWPACK\\Input'
path_joined = os.path.join(path_input, tail)

#Lat, Lon, Altitude of station
df_geo = pd.read_table(path_joined, skiprows=[0,1,5], nrows=3,
                        header=None, sep = ' ', index_col=0)
