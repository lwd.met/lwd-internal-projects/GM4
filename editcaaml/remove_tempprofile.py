#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 08:24:14 2022
Thsís Script should deleate class tempProfile if its not filled by observer. 
Otherwise - R function ReadinCaaml is not able to read in profile without error message. 

@author: hanna
"""
import requests
import os.path
import numpy as np
# import xml.etree.ElementTree as et
from lxml import etree as et
import pandas as pd

file = r'/home/hanna/GITLAB/GM4/editcaaml/testProfilefordelatingtemp/snowprofile6673.xml'
new_file = '6673.caaml'


def removeTempProfile(file, new_file):
    '''
    Remove Class tempProfile if there is no Observer Input 

    Parameters
    ----------
    file / new_file : str
        path to xml file of snowprofile


    Returns nothing, but saves new profile
    -------
    '''
    # - Parse xml object - #
    try:
        tree = et.parse(file)
        xroot = tree.getroot()
        
        
        child_snowProfileResultsOf = xroot.find('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}snowProfileResultsOf')
        child_SnowProfileMeasurements = child_snowProfileResultsOf.find('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}SnowProfileMeasurements')
        child_tempProfile = child_SnowProfileMeasurements.find('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}tempProfile')
        #figure out if there if there is observer Input in class tempProfile. 
        #If there is one - leave unchanged if no observed Temperature profile delet class tempProfile from file. 
        if child_tempProfile.find('{http://caaml.org/Schemas/SnowProfileIACS/v6.0.3}Obs') == None:
            child_SnowProfileMeasurements.remove(child_tempProfile)
        else:
            pass
        
        
    
        tree.write(new_file,encoding='UTF-8', method="xml", doctype='<?xml version="1.0" encoding="UTF-8"?>')
        
    except:
        pass
    
    return 



path_basic_xml = r'/home/hanna/GITLAB/GM4/snowprofiles/output/download/all'
folder = os.listdir(path_basic_xml)
for profile in folder:
    file = r'/home/hanna/GITLAB/GM4/snowprofiles/output/download/all/{}'.format(profile)
    identiy = profile[11:-4]
    new_filename = 'snowprofile{}.caaml'.format(identiy)
    #n_file = '/home/hanna/GITLAB/GM4/editcaaml/editedCaamlProfiles/snowprofilemi.caaml'
    path = r'/home/hanna/GITLAB/GM4/editcaaml/editedCaamlProfiles'
    new_file = os.path.join(path, new_filename)

    
    removeTempProfile(file, new_file)
     
