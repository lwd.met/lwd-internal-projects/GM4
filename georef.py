# -*- coding: utf-8 -*-
"""
Created on Tue May  4 08:52:46 2021

@author: hadis
"""

import os
import matplotlib.pyplot as plt
import pandas as pd
import geopandas as gpd
# import earthpy as et

import shapely.speedups
shapely.speedups.enable()

# Local imports
import si_global_variables as si_var

# After installation as module
# import stability_index.si_global_variables as si_var

path_regions = os.path.join(si_var.path_shape, 'avalanche_report_regions.shp')
# Load the file, because its a default value in function 'pip'
albina_regions = gpd.read_file(path_regions)
selected_cols = ['RegionCode', 'geometry']
albina_regions = albina_regions[selected_cols]

def pip(points, polys=albina_regions, crs_points="EPSG:4326"):
    """
    pip ... Point in Polygon
    Spatial join with points in polygon.


    Parameters
    ----------
    points : Pandas DataFrame
        Points. Requires: "longitude" and "latitude" as column names.
    polys : GeoPandas DataFrame, optional
        Polygons. The default is albina_regions.
    crs_points : str, optional
        Projection of the points. The default is "EPSG:4326".
        see https://geopandas.org/docs/user_guide/projections.html


    Returns
    -------
    GeoPandas DataFrame
        Join of DataFrame points with respective polygons.

    """


    # Create GeoDataFrame
    points = gpd.GeoDataFrame(points, crs=crs_points,
                              geometry=gpd.points_from_xy(points.longitude,
                                                          points.latitude))

    # Make sure poinits have same projection as polys
    points = points.to_crs(polys.crs)

    # Spatial join: points in polys
    points_join = gpd.sjoin(points, polys, how='inner', op='within')

    return(points_join)

#%% For development only
if __name__ == '__main__':
    test_path = os.path.join(si_var.parent_path_regions, 'tyrol',     'tyrol_geo_data.csv')
    df = pd.read_csv(test_path, skiprows=0, index_col=0).T

    crs_points="EPSG:4326"
    df = pd.read_csv(test_path, skiprows=0, index_col=0).T
    gdf = gpd.GeoDataFrame(df, crs=crs_points,
                            geometry=gpd.points_from_xy(df.longitude, df.latitude))
    gdf = gdf.to_crs(albina_regions.crs)
    # gdf = gdf['geometry']

    join = gpd.sjoin(gdf, albina_regions, how='inner', op='within')



    # Plot the points
    fig, ax = plt.subplots(figsize=(10,10))
    #data_haupterverkehrswege.plot(cmap = 'Greys',ax=ax)
    albina_regions.boundary.plot(color=None,edgecolor='k',linewidth = 2,ax=ax)
    #Use your second dataframe
    # gdf.plot(ax=ax)
    gdf.plot(ax=ax)





























